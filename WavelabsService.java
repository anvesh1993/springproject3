package com.example;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WavelabsService {
	@Autowired
	private WavelabsRepository wavelabsRepository;

	public WavelabsEmployee saveEmployee(WavelabsEmployee employee) {
		return wavelabsRepository.save(employee);
	}

	@Cacheable(cacheNames = "mycache")
	public List<WavelabsEmployee> getAllEmployee() {
		
		return wavelabsRepository.findAll();
		}

		@CachePut(cacheNames = "mycache", key = "#employee.id")
		@Transactional(rollbackFor = Exception.class)
		public WavelabsEmployee updateEmployee(WavelabsEmployee employee, int id) {
			employee.setId(id);
			return wavelabsRepository.save(employee);
		}

	}