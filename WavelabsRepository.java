package com.example;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WavelabsRepository extends JpaRepository<WavelabsEmployee, Integer> {

}