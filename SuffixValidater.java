package com.example;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SuffixValidater implements ConstraintValidator<Suffix,String>
{
	private String prefix;
	
	@Override
	public void initialize(Suffix constraints)
	{
		this.prefix=constraints.prefix();
	}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (value.startsWith(prefix)) {
			return true;
}
		
		return false;
	}
	

}