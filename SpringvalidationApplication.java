package com.example;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	@Autowired
	private WavelabsService wavelabsService;
	@PostMapping(value = "/wavelabsemployee", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WavelabsEmployee> saveEmployee(@Valid @RequestBody WavelabsEmployee wavelabsEmployee) {
		return ResponseEntity.status(201).body(wavelabsService.saveEmployee(wavelabsEmployee));
	}

	@GetMapping(value = "/wavelabsemployee", consumes = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	
	public ResponseEntity<List<WavelabsEmployee>> getAllEmployee() {
		List<WavelabsEmployee> employees = wavelabsService.getAllEmployee();
		return ResponseEntity.status(200).body(employees);
	}

	@PutMapping(value = "/wavelabsemployee/{id}")
	public ResponseEntity<WavelabsEmployee> updateEmployee(@RequestBody 
			WavelabsEmployee wavelabsEmployee,
			@PathVariable("id") int id) {
		return ResponseEntity.status(201).body(wavelabsService.updateEmployee(wavelabsEmployee, id));
	}

}