package com.example;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(FIELD)
@Retention(RUNTIME)
//@Repeatable(List.class)
@Documented
@Constraint(validatedBy = { })

public @interface Suffix 
{
	

	Class<?>[] groups() default { };
	Class<? extends Payload>[] payload() default { };
	
	String message() default " please give correct prefix ";
	
	String prefix() default "WL";
	

}